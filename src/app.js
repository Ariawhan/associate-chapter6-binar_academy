const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const routes = require("./routes");

// Intializations
const app = express();

// Settings view engine
app.set("views", __dirname + "/views");
app.set("view engine", "pug");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// app.use(express.json());

// Public
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "controller")));

// Routes
app.use(routes);

module.exports = {
  app,
};
