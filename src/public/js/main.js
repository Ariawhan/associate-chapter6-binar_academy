//Pakek ini buat cookie
// document.cookie = "username=Max Brown; expires=Wed, 05 Aug 2020 23:00:00 UTC";
//Create Cookie
const Cookie = (id, token, role, username) => {
  document.cookie = "id=" + id;
  document.cookie = "token=" + token;
  document.cookie = "role=" + role;
  document.cookie = "username=" + username;
};
// const getCookie = (name) => {
//   var cookieArr = document.cookie.split(";");
//       for(var i = 0; i < cookieArr.length; i++) {
//           var cookiePair = cookieArr[i].split("=");
//           if(name == cookiePair[0].trim()) {
//               // console.log(decodeURIComponent(cookiePair[1]))
//               return decodeURIComponent(cookiePair[1])
//           }
//       }
//   };
// console.log(getCookie("id"))
// Login User
async function Login() {
  const data = {
    username: document.getElementById("usernameinput").value,
    password: document.getElementById("passwordlogin").value,
  };
  if (data.username == "" || data.password == "") {
    alert("Form Tidak Boleh Kosong!");
  } else {
    // console.log(data);
    const restAPI = await axios.post("/api/auth/login", data);
    // console.log(restAPI.data.data.user.id);
    // console.log(restAPI.data.data.user.role);
    if (restAPI.data.status) {
      // status blokir
      if (restAPI.data.data.user.status == 0) {
        if (restAPI.data.data.user.role == "1") {
          // alert("Login Sukses");
          Cookie(
            restAPI.data.data.user.id,
            restAPI.data.data.user.token,
            restAPI.data.data.user.role,
            restAPI.data.data.user.username
          );
          window.location.href = "/admin";
        } else {
          // alert("Login Sukses");
          Cookie(
            restAPI.data.data.user.id,
            restAPI.data.data.user.token,
            restAPI.data.data.user.role,
            restAPI.data.data.user.username
          );
          window.location.href = "/users";
        }
      } else {
        alert("Anda terblokir silakan hubungi admin");
      }
      // Check role admin/users
    } else {
      alert("Username atau password salah");
    }
  }
}

// Register User
async function Register() {
  const data = {
    username: document.getElementById("usernameRegister").value,
    password: document.getElementById("passwordResgiter").value,
  };
  const passwordKonfirmasi =
    document.getElementById("passwordKonfirmasi").value;
  if (data.username == "" || data.password == "" || passwordKonfirmasi == "") {
    alert("Form Tidak Boleh Kosong!");
  } else {
    if (passwordKonfirmasi != data.password) {
      //Check Lagi
      alert("Password Anda Tidak Sama!");
    } else {
      const restAPI = await axios.post("/api/auth/register", data);
      //Status == True
      console.log(restAPI.data.data);
      if (restAPI.data.status) {
        // alert("Behasil");
        // Create cookie
        Cookie(
          restAPI.data.data.id,
          restAPI.data.data.token,
          restAPI.data.data.role,
          restAPI.data.data.username
        );
        window.location.href = "/users";
      } else {
        alert("500 Server Error [Tidak Dapat Menambahkan data]");
      }
    }
  }
}
//Logout with clear all session
function Logout() {
  Cookie("", "", "", "");
  window.location.href = "/";
}

// Add Data Mpvie in dashboard admin
async function AddDataMovie() {
  const data = {
    name: document.getElementById("addMovieNama").value,
    startMovie: document.getElementById("addMovieTanggalMulai").value,
    endMovie: document.getElementById("addMovieTanggalSelesai").value,
    images: document.getElementById("addMovieimages").value,
    seat: document.getElementById("addMovieJumlahKursi").value,
    description: document.getElementById("AddMovieDeskripsi").value,
  };
  console.log(data);
  if (
    data.name == "" ||
    data.startMovie == "" ||
    data.endMovie == "" ||
    data.seat == "" ||
    data.iamges == "" ||
    data.description == ""
  ) {
    alert("Data Tidak Boleh Kosong!");
  } else {
    const restAPI = await axios.post("/api/insertData/addMovie", data);
    if (restAPI.data.status) {
      // alert("Berhasil Tambah Data");
      window.location.href = "/admin";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}

// Edit Data Mpvie in dashboard admin
async function EditDataMovie(idMovie) {
  const data = {
    id: idMovie,
    name: document.getElementById("editMovieNama" + idMovie).value,
    startMovie: document.getElementById("editMovieTanggalMulai" + idMovie)
      .value,
    endMovie: document.getElementById("editMovieTanggalSelesai" + idMovie)
      .value,
    seat: document.getElementById("editMovieJumlahKursi" + idMovie).value,
    images: document.getElementById("editMovieimages" + idMovie).value,
    description: document.getElementById("editMovieDeskripsi" + idMovie).value,
  };
  console.log(data);
  if (
    data.name == "" ||
    data.startMovie == "" ||
    data.endMovie == "" ||
    data.seat == "" ||
    data.description == ""
  ) {
    alert("Data Tidak Boleh Kosong!");
  } else {
    const restAPI = await axios.put("/api/updateData/editMovie", data);
    if (restAPI.data.status) {
      // console.log("Berhasil");
      window.location.href = "/admin";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}

async function Deletemovie(idMovie) {
  console.log(idMovie);
  const restAPI = await axios.delete("/api/deleteData/" + idMovie);
  if (restAPI.data.status) {
    // alert("Berhasil Deleta Data");
    window.location.href = "/admin";
  } else {
    alert("500 Server Error [Tidak Dapat Menambahkan data]");
  }
}

async function OrdersMovie(idDataMovie, idDataUsers) {
  const data = {
    wibuCards: document.getElementById("wibuCards" + idDataMovie).value,
    idFilm: idDataMovie,
    userId: idDataUsers,
  };
  console.log(data.wibuCards.length);
  if (data.wibuCards.length == 16) {
    // Api with parameters :id = /ordersMovie
    const restAPI = await axios.post("/api/insertData/ordersMovie", data);
    if (restAPI.data.status) {
      // alert("Berhasil Tambah Data");
      window.location.href = "/users/orders";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  } else {
    alert("Masukan valid 16 number");
  }
}

async function ChangeRole(idUsers, roleUsers) {
  if (roleUsers == 0) {
    // Get API check lagi
    // Api with parameters :id = /changeRole
    // Query p = 1 -> change to Admin
    // p = 2 -> change to Users
    const restAPI = await axios.put(
      "/api/updateData/changeRole?p=1&q=" + idUsers
    );
    if (restAPI.data.status) {
      // alert("Berhasil");
      window.location.href = "/admin/users";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  } else {
    // Get API check lagi
    // Api with parameters :id = /changeRole
    // Query p = 1 -> change to Admin
    // p = 2 -> change to Users
    const restAPI = await axios.put(
      "/api/updateData/changeRole?p=2&q=" + idUsers
    );
    if (restAPI.data.status) {
      window.location.href = "/admin/users";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}

async function ChangeStatus(idUsers, status) {
  if (status == 0) {
    // Get API check lagi
    // Api with parameters :id = /blokir
    // Query q = 1 -> bloks
    // q = 2 -> unblok
    const restAPI = await axios.put("/api/updateData/status?q=1&p=" + idUsers);
    if (restAPI.data.status) {
      window.location.href = "/admin/users";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  } else {
    // Get API check lagi
    // Api with parameters :id = /changeRole
    // Query q = 1 -> bloks
    // q = 2 -> unblok
    const restAPI = await axios.put("/api/updateData/status?q=2&p=" + idUsers);
    if (restAPI.data.status) {
      window.location.href = "/admin/users";
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}

function searchData() {
  const data = document.getElementById("input-search").value;
  // const restAPI = await axios.put(`/admin?key=${data}`);
  window.location.href = "/admin?key=" + data;
  // window.location.href = "/admin/users";
  // window.location = "https://www.google.com";
  // console.log("pindah");
  // await location.assign("http://www.mozilla.org");
}
