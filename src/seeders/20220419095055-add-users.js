'use strict';
const md5 = require("md5");
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert("users", [{
      username: "admin",
      password: md5("admin"),
      status: 0, 
      role: 1, //role 1= admin role 0=user 
      token: md5(md5("admin") + md5(md5("12345")) + md5(new Date())),
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      username: "user",
      password: md5("user"),
      status: 0,
      role: 0,
      token: md5(md5("admin") + md5(md5("12345")) + md5(new Date())),
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
