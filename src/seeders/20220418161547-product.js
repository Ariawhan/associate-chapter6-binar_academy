"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "products",
      [
        {
          name: "Boruto: Naruto Next Generations",
          desc: "Boruto: Naruto Next Generations adalah sebuah seri manga asal Jepang yang ditulis oleh Ukyo Kodachi dan Masashi Kishimoto dan diilustrasikan oleh Mikio Ikemoto.",
          images:
            "https://pict-a.sindonews.net/dyn/850/pena/news/2021/10/27/700/581850/10-jurus-terkuat-boruto-yang-berguna-untuk-masa-depannya-gqm.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 50000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "One Piece",
          desc: "One Piece is a Japanese manga series written and illustrated by Eiichiro Oda. It has been serialized in Shueisha's shōnen manga magazine Weekly Shōnen Jump since July 1997, with its individual chapters compiled into 102 tankōbon volumes as of April 2022.",
          images:
            "https://mmc.tirto.id/image/otf/1024x535/2018/08/01/film-one-piece-wikipedia.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 50000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Your Name",
          desc: "Your Name is a 2016 Japanese animated romantic fantasy film produced by CoMix Wave Films and released by Toho. It depicts a high school boy in Tokyo and a high school girl in the Japanese countryside who suddenly and inexplicably begin to swap bodies.",
          images:
            "https://cdn.medcom.id/images/content/2018/01/09/813685/hm8sMwlDoz.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 70000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Weathering with You",
          desc: "Weathering with You (Japanese: 天気の子, Hepburn: Tenki no Ko, lit. Child of Weather) is a 2019 Japanese animated romantic fantasy film produced by CoMix Wave Films and released by Toho. It depicts a high school boy who runs away from his rural home to Tokyo and befriends an orphan girl who has the ability to control the weather.",
          images:
            "https://akcdn.detik.net.id/visual/2019/08/07/bec1104c-f7b1-4a7a-82a5-28f37c4d1d40_169.jpeg?w=650",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 40000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Violet Evergarden: The Movie",
          desc: "Violet Evergarden: The Movie (Japanese: 劇場版 ヴァイオレット・エヴァーガーデン, Hepburn: Gekijōban Vaioretto Evāgāden) is a 2020 Japanese animated film based on Violet Evergarden light novel series by Kana Akatsuki. Produced by Kyoto Animation and distributed by Shochiku, the film is directed by Taichi Ishidate from a script written by Reiko Yoshida, and stars Yui Ishikawa and Daisuke Namikawa. In the film, Violet Evergarden continues in her search for the meaning of the final words left by Gilbert Bougainvillea when she receives a request to write a letter from a boy named Yuris.",
          images:
            "https://akcdn.detik.net.id/api/wm/2021/03/03/violet-evergarden_169.jpeg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 80000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "No Game No Life: Zero",
          desc: "No Game No Life: Zero (Japanese: ノーゲーム・ノーライフ ゼロ, Hepburn: Nōgēmu Nōraifu Zero) is a Japanese animated film based on The Gamer Couple Who Challenged the World!, the sixth volume of the light novel series No Game No Life by Yuu Kamiya. The film was directed by Atsuko Ishizuka at studio Madhouse. It premiered in Japan on July 15, 2017. The film has been licensed by Sentai Filmworks in North America, Madman Entertainment in Australia and New Zealand, and by MVM in the United Kingdom and Ireland",
          images: "https://i.ytimg.com/vi/quj8sK3Phh8/maxresdefault.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 90000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Demon Slayer: Kimetsu no Yaiba - The Movie: Mugen Train",
          desc: "Demon Slayer: Kimetsu no Yaiba - The Movie: Mugen Train (Japanese: 劇場版「鬼滅の刃」 無限列車編, Hepburn: Gekijō-ban Kimetsu no Yaiba Mugen Ressha-hen), also known as Demon Slayer: Mugen Train or Demon Slayer: Infinity Train, is a 2020 Japanese animated dark fantasy action film[3][4] based on the shōnen manga series Demon Slayer: Kimetsu no Yaiba by Koyoharu Gotouge. The film, which is a direct sequel to the first season of the anime television series, was directed by Haruo Sotozaki and written by Ufotable staff members. The film was produced by Ufotable in association with Aniplex and Shueisha.",
          images:
            "https://thumbor.prod.vid.id/Pp-h4tbSm3bItAAUqYGmK-UEEcA=/filters:quality(70)/vidio-web-prod-film/uploads/film/image_portrait/4000/demon-slayer-kimetsu-no-yaiba-entertainment-district-arc-bbf986.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 40000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "The Seven Deadly Sins: Cursed By Light",
          desc: "The Seven Deadly Sins: Cursed by Light (七つの大罪：光に呪われし者たち, Nanatsu no Taizai: Hikari ni Norowareshi Mono-tachi) is a 2021 Japanese animated fantasy film, and the second film based on The Seven Deadly Sins manga series written and illustrated by Nakaba Suzuki, and the second installment in The Seven Deadly Sins, and the sequel to the 2018 film The Seven Deadly Sins the Movie: Prisoners of the Sky. The film was released on July 2, 2021 in Japan, which was followed by its Netflix streaming debut on October 1. The film takes place between episodes 23 and 24 of the fourth season and covers the manga's last chapter.",
          images:
            "https://sportshub.cbsistatic.com/i/2021/08/09/d97de1b5-5e2e-43e8-bab9-0bc4b94d38d3/the-seven-deadly-sins-cursed-by-light-movie-1272876.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 10000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Dragon Ball Super: Broly",
          desc: "Dragon Ball Super: Broly (Japanese: ドラゴンボール超スーパー ブロリー, Hepburn: Doragon Bōru Sūpā: Burorī) is a 2018 Japanese anime martial arts fantasy adventure film,[7][8] directed by Tatsuya Nagamine and written by Dragon Ball series creator Akira Toriyama. It is a reboot of Dragon Ball Z: Broly – The Legendary Super Saiyan, the twentieth Dragon Ball feature film overall, the third film produced with Toriyama's direct involvement, and the first to carry the Dragon Ball Super branding.",
          images:
            "https://pict-b.sindonews.net/dyn/480/content/2019/02/19/165/1380090/review-film-dragon-ball-super-broly-ohR-thumb.jpg",
          start_date: new Date().toISOString().split("T")[0],
          end_date: new Date().toISOString().split("T")[0],
          price: 30000,
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
