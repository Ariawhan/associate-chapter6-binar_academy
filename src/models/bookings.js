"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class bookings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      bookings.belongsTo(models.products, {
        foreignKey: "id_film",
      });
    }
  }
  bookings.init(
    {
      id_film: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      card: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "bookings",
    }
  );
  return bookings;
};
