const auth = require("./auth.js");
const dashboard = require("./dashboard.js");

module.exports = {
  auth,
  dashboard,
};
