const { products } = require("../models");
const { users } = require("../models");
const { bookings } = require("../models");
const sequelize = require("sequelize");

module.exports = {
  getAdminDataUndefined(reqBody) {
    return products.findAll({
      where: sequelize.where(
        sequelize.fn("lower", sequelize.col("name")),
        "Like",
        "%" + reqBody + "%"
      ),
    });
  },
  getAdminData() {
    return products.findAll({});
  },
  getUsersData() {
    return users.findAll({});
  },
  getMovieData() {
    return products.findAll({});
  },
  getBookingsData() {
    return bookings.findAll({});
  },
  getOrdersData(reqBody) {
    // SELECT products.name, products.price, bookings.card
    // FROM bookings
    // INNER JOIN products ON bookings.id_film = products.id
    // WHERE bookings.user_id = 2
    // const ordersData = await bookings.findAll({});
    return bookings.findAll({
      where: {
        user_id: reqBody,
      },
      include: {
        model: products,
        attributes: ["name", "price"],
      },
    });
  },
  updateDataMovie(reqBody, idData) {
    return products.update(reqBody, {
      where: {
        id: idData,
      },
    });
  },
  updateRoleUsers(roleChange, idData) {
    return users.update(
      {
        role: roleChange,
      },
      {
        where: {
          id: idData,
        },
      }
    );
  },
  updateStatusUsers(statusChange, idData) {
    return users.update(
      {
        status: statusChange,
      },
      {
        where: {
          id: idData,
        },
      }
    );
  },
  deleteDataMovie(idData) {
    return products.destroy({
      where: {
        id: idData,
      },
    });
  },
  insertDataMovie(reqBody) {
    return products.create(reqBody);
  },
  insertDataBookings(reqBody) {
    bookings.create(reqBody);
  },
  getCookie(name, req) {
    var cookieArr = req.split(";");
    for (var i = 0; i < cookieArr.length; i++) {
      var cookiePair = cookieArr[i].split("=");
      if (name == cookiePair[0].trim()) {
        return decodeURIComponent(cookiePair[1]);
      }
    }
  },
};
