const { users } = require("../models");
const md5 = require("md5");
const { Op } = require("sequelize");

module.exports = {
  //login
  loginUsers(reqBody) {
    // SELECT username, id, token FROM users WHERE username='ariawan' && password="asas"
    reqBody.password = md5(reqBody.password);
    return users.findOne({
      attributes: ["username", "id", "token", "role", "status"],
      where: {
        [Op.and]: [
          { username: reqBody.username },
          { password: reqBody.password },
        ],
      },
    });
  },
  //create
  createUser(reqBody) {
    // INSERT INTO "users" ("id","username", "password", "token", "createdAt", "updatedAt") VALUES (DEFAULT,'Ramadhan', 'e10adc3949ba59abbe56e057f20f883e', '34e1b5f421250aaa4ef44e30a9eb0a64', NOW(),NOW())
    return users.create({
      username: reqBody.username,
      password: md5(reqBody.password),
      status: 0, // Status di blokir apa tidak (tambah di database)
      role: 0, // role admin apa gak admin (tambah di daabase)
      token: md5(
        md5(reqBody.username) + md5(reqBody.password) + md5(new Date())
      ),
    });
  },
};
