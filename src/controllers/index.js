const controllersAuth = require("./auth");
const controllersDashboard = require("./dashboard");

module.exports = {
  controllersAuth,
  controllersDashboard,
};
