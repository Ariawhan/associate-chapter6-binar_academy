const { dashboard } = require("../services");

// Get data
const getAdmin = async (req, res) => {
  if (req.query.key !== undefined) {
    const data = req.query.key.toLowerCase();
    try {
      // SELECT SUM(CAST(products.price as int)) as total
      // FROM bookings
      // INNER JOIN products ON bookings.id_film = products.id
      const totalMovie = await dashboard.getMovieData();
      const totalUser = await dashboard.getUsersData();
      const totalBooking = await dashboard.getBookingsData();
      // console.log(totalMovie);
      const searchResult = await dashboard.getAdminDataUndefined(data);
      // console.log(searchResult);
      res.render("../views/admin/dashboard", {
        status: true,
        message: "Data Movie",
        data: searchResult,
        username: dashboard.getCookie("username", req.headers.cookie),
        role: dashboard.getCookie("role", req.headers.cookie),
        idUsers: dashboard.getCookie("id", req.headers.cookie),
        token: dashboard.getCookie("token", req.headers.cookie),
        totalMovie: totalMovie.length,
        totalUser: totalUser.length,
        totalBooking: totalBooking.length,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  }
  // console.log(req.query.key);
  else {
    try {
      const movieData = await dashboard.getMovieData();
      const totalMovie = await dashboard.getMovieData();
      const totalUser = await dashboard.getUsersData();
      const totalBooking = await dashboard.getBookingsData();
      res.render("../views/admin/dashboard", {
        status: true,
        message: "Data Movie",
        data: movieData,
        username: dashboard.getCookie("username", req.headers.cookie),
        role: dashboard.getCookie("role", req.headers.cookie),
        idUsers: dashboard.getCookie("id", req.headers.cookie),
        token: dashboard.getCookie("token", req.headers.cookie),
        totalMovie: totalMovie.length,
        totalUser: totalUser.length,
        totalBooking: totalBooking.length,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  }
};

const getUsers = async (req, res) => {
  try {
    const usersData = await dashboard.getUsersData();
    // console.log(usersData);
    res.render("../views/admin/users", {
      status: true,
      message: "Data Users",
      data: usersData,
      username: dashboard.getCookie("username", req.headers.cookie),
      role: dashboard.getCookie("role", req.headers.cookie),
      idUsers: dashboard.getCookie("id", req.headers.cookie),
      token: dashboard.getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const getMovie = async (req, res) => {
  try {
    const movieData = await dashboard.getMovieData();
    res.render("../views/customer/cards", {
      status: true,
      message: "Data Movie",
      data: movieData,
      username: dashboard.getCookie("username", req.headers.cookie),
      role: dashboard.getCookie("role", req.headers.cookie),
      idUsers: dashboard.getCookie("id", req.headers.cookie),
      token: dashboard.getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const getOrders = async (req, res) => {
  try {
    const usersID = dashboard.getCookie("id", req.headers.cookie);
    const ordersData = await dashboard.getOrdersData(usersID);
    // console.log(ordersData);
    res.render("../views/customer/oders", {
      status: true,
      message: "Data Orders",
      data: ordersData,
      username: dashboard.getCookie("username", req.headers.cookie),
      role: dashboard.getCookie("role", req.headers.cookie),
      idUsers: dashboard.getCookie("id", req.headers.cookie),
      token: dashboard.getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

// Update data
const updateData = async (req, res) => {
  if (req.params.id === "editMovie") {
    const idData = req.body.id;
    const data = {
      name: req.body.name,
      start_date: req.body.startMovie,
      end_date: req.body.endMovie,
      price: req.body.seat,
      images: req.body.images,
      desc: req.body.description,
    };
    try {
      const usersData = await dashboard.updateDataMovie(data, idData);
      if (parseInt(usersData) == 0) {
        console.log(parseInt(usersData));
        res.status(200).json({
          status: false,
          message: `Invalid id movie ${id}`,
        });
      } else {
        console.log(usersData);
        res.status(200).json({
          status: true,
          message: "Movie success edited",
        });
      }
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else if (req.params.id === "changeRole") {
    // Query p = 1 -> change to Admin
    // p = 2 -> change to Users
    const idData = req.query.q;
    switch (parseInt(req.query.p)) {
      case 1:
        try {
          await dashboard.updateRoleUsers(1, idData);
          res.status(200).json({
            status: true,
            message: "Successful change role to Admin",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
      case 2:
        try {
          await dashboard.updateRoleUsers(0, idData);
          res.status(200).json({
            status: true,
            message: "Successful change role to user",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
    }
  } else if (req.params.id === "status") {
    // Query q = 1 -> bloks
    // q = 2 -> unblok
    const idData = req.query.p;
    console.log(idData);
    console.log(parseInt(req.query.q));
    switch (parseInt(req.query.q)) {
      case 1:
        try {
          await dashboard.updateStatusUsers(1, idData);
          res.status(200).json({
            status: true,
            message: "Successful blokir",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
      case 2:
        try {
          await dashboard.updateStatusUsers(0, idData);
          res.status(200).json({
            status: true,
            message: "Successful unblokir",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
    }
  }
};

// Delete data
const deleteData = async (req, res) => {
  try {
    const id = req.params.id;
    console.log(id);
    const deleted = await dashboard.deleteDataMovie(id);
    if (deleted == 0) {
      console.log(deleted);
      res.status(200).json({
        status: false,
        message: `Invalid id movie ${id}`,
      });
    } else {
      res.status(200).json({
        status: true,
        message: `Successful deleted movie id ${id}`,
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

// Insert data
const insertData = async (req, res) => {
  if (req.params.id === "addMovie") {
    const data = {
      name: req.body.name,
      start_date: req.body.startMovie,
      end_date: req.body.endMovie,
      price: req.body.seat,
      images: req.body.images,
      desc: req.body.description,
    };
    try {
      const newProduct = await dashboard.insertDataMovie(data);
      res.status(201).json({
        status: true,
        message: "Movie created successfully",
        data: {
          newProduct,
        },
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else if (req.params.id === "ordersMovie") {
    if (req.body.wibuCards.length == 16) {
      const data = {
        id_film: req.body.idFilm,
        user_id: req.body.userId,
        card: req.body.wibuCards,
      };
      try {
        const booking = await dashboard.insertDataBookings(data);
        res.status(201).json({
          status: true,
          message: "Booking created successfully",
          data: {
            booking,
          },
        });
      } catch (err) {
        res.status(400).json({
          status: "fail",
          errors: [err.message],
        });
      }
    } else {
      res.status(201).json({
        status: false,
        message: "Invalid Card Number",
      });
    }
  }
};

module.exports = {
  getAdmin,
  getUsers,
  getMovie,
  getOrders,
  updateData,
  deleteData,
  insertData,
};
