const { auth } = require("../services");

const authLogin = async (req, res) => {
  const data = {
    username: req.body.username,
    password: req.body.password,
  };
  try {
    const user = await auth.loginUsers(data);
    // console.log(user);
    if (user == null) {
      res.status(200).json({
        status: false,
        message: "Username or Password incorrect!",
      });
    } else {
      res.status(200).json({
        status: true,
        message: "Login Successfully",
        data: {
          user,
        },
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const createUser = async (req, res) => {
  const data = {
    username: req.body.username,
    password: req.body.password,
  };
  try {
    const newUser = await auth.createUser(data);
    res.status(201).json({
      status: true,
      message: "Registration Successfully",
      data: {
        id: newUser.dataValues.id,
        username: newUser.dataValues.username,
        token: newUser.dataValues.token,
        role: newUser.dataValues.role,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

module.exports = {
  authLogin,
  createUser,
};
